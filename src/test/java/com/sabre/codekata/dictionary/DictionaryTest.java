package com.sabre.codekata.dictionary;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

public class DictionaryTest {
    
    @Test
    public void testInit4Letters() {
        Dictionary dict = new DictionaryImpl();
        dict.init(4, "src/main/resources/testWordlist.txt");
        Set<String> set = dict.getWords();
        assertEquals(6, set.size());
    }
    
    @Test
    public void testInit3Letters() {
        Dictionary dict = new DictionaryImpl();
        dict.init(3, "src/main/resources/testWordlist.txt");
        Set<String> set = dict.getWords();
        assertEquals(4, set.size());
    }
    
    @Test
    public void testLowerCase() {
        Dictionary dict = new DictionaryImpl();
        dict.init(5, "src/main/resources/testWordlist.txt");
        Set<String> set = dict.getWords();
        assertEquals(1, set.size());
        String horse = set.iterator().next();
        assertEquals("horse", horse);
    }
}
