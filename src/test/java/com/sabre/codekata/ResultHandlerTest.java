package com.sabre.codekata;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;

public class ResultHandlerTest {

    @Test
    public void test() {
        Node a = new Node(null, 0, "a");
        Node b = new Node(a, 1, "b");
        Node c = new Node(b, 2, "c");
        Node d = new Node(c, 3, "d");
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(d);
        assertEquals(Arrays.asList(a.word, b.word, c.word, d.word), rh.getResult());
    }
    
    @Test
    public void testNull() {
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(null);
        assertNotNull(rh.getResult());
        assertTrue(rh.getResult().isEmpty());
    }
}
