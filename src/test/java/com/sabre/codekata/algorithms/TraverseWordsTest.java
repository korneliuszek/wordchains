package com.sabre.codekata.algorithms;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import com.sabre.codekata.DefaultResultHandler;
import com.sabre.codekata.ResultHandler;
import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;
import com.sabre.codekata.dictionary.Dictionary;
import com.sabre.codekata.dictionary.DictionaryImpl;

public class TraverseWordsTest {

    @Test
    public void testCatDog() {
        TraverseWords tw = new TraverseWordsImpl();
        Dictionary dict = new DictionaryImpl();
        dict.init(3, "src/main/resources/testWordlist.txt");
        WordGenerator wg = new WordGeneratorImpl(dict);
        Node node = tw.traverse("cat", "dog", wg);
        assertNotNull(node);
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(node);
        rh.handleResult();
        assertEquals(Arrays.asList("cat", "cot", "cog", "dog"), rh.getResult());
    }

    @Test
    public void testRuby() {
        TraverseWords tw = new TraverseWordsImpl();
        Dictionary dict = new DictionaryImpl();
        dict.init(4, "src/main/resources/testWordlist.txt");
        WordGenerator wg = new WordGeneratorImpl(dict);
        Node node = tw.traverse("ruby", "code", wg);
        assertNotNull(node);
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(node);
        rh.handleResult();
        assertEquals(Arrays.asList("ruby", "rubs", "robs", "rods", "rode", "code"), rh.getResult());
    }

    @Test
    public void testRubyFullList() {
        TraverseWords tw = new TraverseWordsImpl();
        Dictionary dict = new DictionaryImpl();
        dict.init(4, "src/main/resources/wordlist.txt");
        WordGenerator wg = new WordGeneratorImpl(dict);
        Node node = tw.traverse("ruby", "code", wg);
        assertNotNull(node);
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(node);
        rh.handleResult();
        assertEquals(Arrays.asList("ruby", "roby", "robe", "rode", "code"), rh.getResult());
    }

    @Test
    public void testSame() {
        TraverseWords tw = new TraverseWordsImpl();
        Dictionary dict = new DictionaryImpl();
        dict.init(4, "src/main/resources/wordlist.txt");
        WordGenerator wg = new WordGeneratorImpl(dict);
        Node node = tw.traverse("ruby", "ruby", wg);
        assertNotNull(node);
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(node);
        rh.handleResult();
        assertEquals(Arrays.asList("ruby"), rh.getResult());
    }

    @Test
    public void testEmptySolution() {
        TraverseWords tw = new TraverseWordsImpl();
        Dictionary dict = new DictionaryImpl();
        dict.init(4, "src/main/resources/wordlist.txt");
        WordGenerator wg = new WordGeneratorImpl(dict);
        Node node = tw.traverse("asdadadsasd", "abcdabcdabc", wg);
        assertNull(node);
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(node);
        rh.handleResult();
        assertTrue(rh.getResult().isEmpty());
    }

}
