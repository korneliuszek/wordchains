package com.sabre.codekata.algorithms;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import com.sabre.codekata.algorithms.WordGeneratorImpl;
import com.sabre.codekata.dictionary.Dictionary;
import com.sabre.codekata.dictionary.DictionaryImpl;

public class WordGeneratorTest {

    private static final String WORDLIST_PATH = "src/main/resources/wordlist.txt";
    private static final String TEST_WORDLIST_PATH = "src/main/resources/testWordlist.txt";

    @Test
    public void testCat() {
        Dictionary dict = new DictionaryImpl();
        dict.init(3, TEST_WORDLIST_PATH);
        WordGenerator wd = new WordGeneratorImpl(dict);
        Set<String> words = wd.getWordsFor("cat");
        words.stream().forEach(System.out::println);
        assertEquals(1, words.size());
    }
    
    @Test
    public void testRuby() {
        Dictionary dict = new DictionaryImpl();
        dict.init(4, TEST_WORDLIST_PATH);
        WordGenerator wd = new WordGeneratorImpl(dict);
        Set<String> words = wd.getWordsFor("ruby");
        words.stream().forEach(System.out::println);
        assertEquals(1, words.size());
    }
    
    @Test
    public void testHorse() {
        Dictionary dict = new DictionaryImpl();
        dict.init(5, WORDLIST_PATH);
        WordGenerator wd = new WordGeneratorImpl(dict);
        Set<String> words = wd.getWordsFor("horse");
        assertEquals(16, words.size());
    }
}
