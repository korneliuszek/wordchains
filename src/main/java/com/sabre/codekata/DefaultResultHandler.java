package com.sabre.codekata;

import java.util.ArrayList;
import java.util.List;

import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;

public class DefaultResultHandler implements ResultHandler {

    private List<String> result = new ArrayList<>();
    
    @Override
    public boolean acceptNode(Node node) {
        while(node != null) {
            result.add(0, node.word);
            node = node.previous;
        }
        return true;
    }
    
    @Override
    public void handleResult() {
        if(result.size() == 0) {
            System.out.println("No result found.");
        }
        System.out.println("Soultion size: " + result.size());
        for(String s : result) {
            System.out.println(s);
        }
    }

    @Override
    public List<String> getResult() {
        return result;
    }

}
