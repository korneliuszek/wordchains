package com.sabre.codekata;

import com.sabre.codekata.algorithms.TraverseWordsImpl;
import com.sabre.codekata.algorithms.WordGeneratorImpl;
import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;
import com.sabre.codekata.dictionary.Dictionary;
import com.sabre.codekata.dictionary.DictionaryImpl;

public class WordChainsMain {
    public static void main(String[] args) {
        String start;
        String end;
        if(args.length < 2) {
            throw new RuntimeException("Please provide 2 args");
        }
        start = args[0];
        end = args[1];
        if(start.length() != end.length()) {
            throw new RuntimeException("Start and end words should be the same length.");
        }
        Dictionary dict = new DictionaryImpl();
        dict.init(start.length(), "src/main/resources/wordlist.txt");
        WordGeneratorImpl wg = new WordGeneratorImpl(dict);
        TraverseWordsImpl tw = new TraverseWordsImpl();
        Node result = tw.traverse(start, end, wg);
        ResultHandler rh = new DefaultResultHandler();
        rh.acceptNode(result);
        rh.handleResult();
    }
    
} 

