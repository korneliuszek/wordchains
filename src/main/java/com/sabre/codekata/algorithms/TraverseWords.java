package com.sabre.codekata.algorithms;

import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;

public interface TraverseWords {

    Node traverse(String start, String end, WordGenerator wg);

}