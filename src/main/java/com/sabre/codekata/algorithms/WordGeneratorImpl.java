package com.sabre.codekata.algorithms;

import java.util.HashSet;
import java.util.Set;

import com.sabre.codekata.dictionary.Dictionary;
import com.sabre.codekata.dictionary.DictionaryImpl;

public class WordGeneratorImpl implements WordGenerator {

    private Dictionary dict;

    public WordGeneratorImpl(Dictionary dict) {
        this.dict = dict;
    }

    /* (non-Javadoc)
     * @see com.sabre.codekata.algorithms.WordGenerator#getWordsFor(java.lang.String)
     */
    @Override
    public Set<String> getWordsFor(String word) {
        Set<String> words = dict.getWords();
        Set<String> result = new HashSet<>();
        for (int i = 0; i < word.length(); i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                char[] arr = word.toCharArray();
                if(arr[i] != c) {
                    arr[i] = c;
                    String newWord = String.valueOf(arr);
                    if(words.contains(newWord)) {
                        result.add(newWord);
                    }
                }
            }
        }
        return result;
    }
    
    public static void main() {
        DictionaryImpl d = new DictionaryImpl();
        d.init(3, "src/main/resources/wordlist.txt");
        WordGeneratorImpl wg = new WordGeneratorImpl(d);
        Set<String> words = wg.getWordsFor("cat");
        for(String w : words) {
            System.out.println(w);
        }
    }
}
