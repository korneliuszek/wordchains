package com.sabre.codekata.algorithms;

import java.util.Set;

public interface WordGenerator {

    Set<String> getWordsFor(String word);

}