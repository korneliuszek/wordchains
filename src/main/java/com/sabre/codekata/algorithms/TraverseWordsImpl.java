package com.sabre.codekata.algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import com.sabre.codekata.ResultHandler;

public class TraverseWordsImpl implements TraverseWords {

    public static class Node {
        public Node previous;
        public int depth;
        public String word;
        public Node(Node previous, int depth, String word) {
            this.previous = previous;
            this.depth = depth;
            this.word = word;
        }
        public Node(){}
        @Override
        public int hashCode() {
            return word.hashCode();
        }
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Node other = (Node) obj;
            if (word == null) {
                if (other.word != null)
                    return false;
            } else if (!word.equals(other.word))
                return false;
            return true;
        };
        
    }
    
    @Override
    public Node traverse(String start, String end, WordGenerator wg) {
        Queue<Node> queue = new LinkedList<>();
        Node s = new Node(null, 0, start);
        queue.add(s);
        Set<Node> visited = new HashSet<>();
        while (!queue.isEmpty()) {
            Node current = queue.remove();
            if(current.word.equals(end)) {
                return current;
            }
            if(visited.contains(current)) {
                continue;
            }
            Set<String> words = wg.getWordsFor(current.word);
            for(String w : words) {
                queue.add(new Node(current,current.depth +1, w));
            }
            visited.add(current);
        }
        return null;
    }
    
    
}
