package com.sabre.codekata.dictionary;

import java.util.Set;

public interface Dictionary {

    void init(int length, String filePath);
    Set<String> getWords();

}