package com.sabre.codekata.dictionary;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.sabre.codekata.algorithms.TraverseWordsImpl;
import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;
import com.sabre.codekata.algorithms.WordGeneratorImpl;

public class DictionaryImpl implements Dictionary {
    private List<String> lines = new ArrayList<>();

    @Override
    public void init(int length, String filePath) {
        try {
            File file = new File(filePath);
            lines = Files.lines(file.toPath(), Charset.forName("ISO-8859-1"))
                    .filter(s -> s.length() == length).map(s -> s.toLowerCase()).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Set<String> getWords() {
        return new HashSet<String>(lines);
    }
}
