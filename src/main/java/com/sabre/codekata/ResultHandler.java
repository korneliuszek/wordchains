package com.sabre.codekata;

import java.util.List;

import com.sabre.codekata.algorithms.TraverseWordsImpl.Node;

public interface ResultHandler {

    public boolean acceptNode(Node node);
    public List<String> getResult();
    public void handleResult();
}
