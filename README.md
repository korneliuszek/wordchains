Repository for Word Chains project.
This program is expected to solve problem defined here: http://codekata.com/kata/kata19-word-chains/

Algorithm for this program is in class:
com.sabre.codekata.algorithms.TraverseWordsImpl

Please look at the tests to see how it works.

Adjacent word generation can be found in WordGenerator in the same package.